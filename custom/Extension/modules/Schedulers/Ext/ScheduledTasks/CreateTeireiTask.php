<?php

$job_strings[] = 'CreateTeireiTask';

function CreateTeireiTask(){

    require_once("custom/modules/ZK4_Operation/GetNextPlanDay.php");
    require_once('modules/ZK4_Operation/ZK4_Operation.php');

    //次回予定日が本日以降のものを抽出
    
    $query = new SugarQuery();
    $query->select(array('id','teirei_end_c'));
    $query->from(BeanFactory::getBean('ZK4_Operation'));
    $today = date("Y-m-d");
    $query->where()->lte('teirei_nextday_c', $today);
    $query->where()->lte('status', "03");
    $rows = $query->execute();

    $gpd = new GetNextPlanDay;

    
    foreach ($rows as $row) {
        if(empty($row['teirei_end_c']) || $row['teirei_end_c'] >= $today){
            $bean = BeanFactory::retrieveBean('ZK4_Operation', $row['id']);
            $bean->save();        
        }
    }
    
    return true;
}