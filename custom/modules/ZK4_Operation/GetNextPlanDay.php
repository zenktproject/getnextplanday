<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

require_once('data/BeanFactory.php');
require_once('modules/ZK4_Operation/ZK4_Operation.php');
require_once('modules/ZK7_HolidayM/ZK7_HolidayM.php');
require_once('modules/Tasks/Task.php');
require_once('modules/ZK8_op_team/ZK8_op_team.php');


// repeat_class_c : 繰り返し区分
//comit_sdate_c : 確定開始日
//holiday_control_c : 休業日の調整
//teirei_weeks_c : 毎週
//teirei_week_c : 曜日
//teirei_day_c : 日付
//teirei_nweek_c : 第X週
//teirei_businessday_c : 第X営業日
//teirei_lastday_c : 定例最終日 
//teirei_nextday_c :次回予定日
//teirei_end_c :定例終了日


class GetNextPlanDay {

    private $weekday = array('日', '月', '火', '水', '木', '金', '土'); //曜日判定用配列
    private $engWeekday = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'); //曜日判定用（英語）配列

    public function getNextPlanDayLH(&$bean, $event, $arguments) {
        //今日から14日後の日付を取得
        if(empty($bean->id) || $bean->status !="03" || $bean->irai_c != "teirei"){
            return true;
        }
        $day14 = strtotime("+14 day");
        if(!empty($bean->teirei_end_c) && strtotime($bean->teirei_end_c) < $day14){
            $dayend = strtotime($bean->teirei_end_c);
        }else{
            $dayend = $day14;
        }

        do {
            $day = $this->getNextPlanDayMain($bean);
            if(!empty($bean->teirei_end_c) && !empty($bean->teirei_nextday_c) && ($bean->teirei_end_c < $bean->teirei_nextday_c)){
                    break;
            }
            if(strtotime($day) <= $dayend){
                $tsk = new Task();
                $tsk->zk4_operation_tasks_1 = $bean->id;
                if(!empty($bean->user_id_c)){
                    $tsk->user_id_c = $bean->user_id_c; //一次担当
                }
                if(!empty($bean->user_id1_c)){
                    $tsk->user_id1_c = $bean->user_id1_c;//二次担当
                }
                
                $tsk->name = $bean->name;
                $tsk->task_date_c = $day;
                $tsk->task_time_c = $bean->kakutei_time_c;
                $tsk->status = "03";
                 
                $idd = $tsk->save();

                $bean->load_relationship('zk4_operation_tasks_1');

                $bean->zk4_operation_tasks_1->add($idd);
                $bean->teirei_lastday_c = $day;
                
                $team = new ZK8_op_team();
                $team->retrieve($bean->zk8_op_team_zk4_operationzk8_op_team_ida);
                $team->load_relationship('zk8_op_team_tasks_1');
                $team->zk8_op_team_tasks_1->add($idd);

                

            }else{
                //$GLOBALS['log']->fatal("デイ：$day");
                if(empty($bean->teirei_end_c) || $bean->teirei_end_c >= $day){
                    $bean->teirei_nextday_c = $day;
                }
                
                break;
            }
        } while(1);
        return $day;
    }

    //次回予定日取得 main 関数
    //recordId と基準日？(standardDate)が引数？
    //public function getNextPlanDayMain($recordId, $standardDate){
    public function getNextPlanDaySC($recordId) {
        //オペレーション依頼のレコードId からbean を生成
        $bean = BeanFactory::getBean('ZK4_Operation', $recordId);
        return getNextPlanDayMain($bean);
    }

    public function getNextPlanDayMain($bean) {
        //最終実施日が空の場合は確定開始日を設定
        if(empty($bean->id) || $bean->status !="03" || $bean->irai_c != "teirei"){
            return true;
        }
        if (empty($bean->teirei_lastday_c)) {
            $last_day = $bean->kakutei_date_c;
            $new_flag = true;
        } else {
            $last_day = $bean->teirei_lastday_c;
            $new_flag = false;
        }


        //繰り返し区分
        $repeatKbn = $bean->repeat_class_c;

        //繰り返し区分を見て、処理を振り分ける
        if ($repeatKbn === "everyDay") {
            //毎日⇒次の日を取得
            $nextDay = $this->geteveryDay($bean->holiday_control_c, $last_day, $new_flag);
        } else if ($repeatKbn === "dayWeek") {
            //指定の曜日
            $teirei_weeks = unencodeMultienum($bean->teirei_weeks_c);
//            $GLOBALS['log']->fatal(print_r($teirei_weeks, true));
            $nextDay = $this->getDayWeekDay($teirei_weeks, $bean->holiday_control_c, $last_day, $new_flag);
        } else if ($repeatKbn === "date") {
            $nextDay = $this->getNextMonthDay($bean->teirei_day_c, $bean->holiday_control_c, $last_day, $new_flag);
        } else if ($repeatKbn === "businessDay") {
            $nextDay = $this->getBusinessDay($bean->teirei_businessday_c, $last_day, $new_flag);
        } else if ($repeatKbn === "biweekly") {
            //隔週
            //初回実行日を基準日として、隔週日付を取得
            $nextDay = $this->getBiweeklyDay($bean->teirei_week_c, $last_day, $bean->holiday_control_c, $new_flag);
        } else if ($repeatKbn === "nWeek") {
            //第〇週　△曜日 日付取得
            $teirei_nweek = unencodeMultienum($bean->teirei_nweek_c);
            $nextDay = $this->getnWeekDay($teirei_nweek, $bean->teirei_week_c, $last_day, $bean->holiday_control_c, $new_flag);
        } else if ($repeatKbn === "irregular") {
            //不定期
            //通常では入らないが、入った場合はfalseを返す
            return false;
        }
        return $nextDay;
    }

//////////////	
    //毎日 日付取得（次の日を取得）
    //引数： 
    public function geteveryDay($holiday_control, $teirei_lastday, $new_flag = false) {
        $nextDay = $teirei_lastday;

        if($new_flag && ($holiday_control == '0' || ($this->chkBussinessDay($nextDay)))){
            return $nextDay = date("Y-m-d", strtotime($nextDay));
        }
        
        do {
            //確定開始日の次の日を取得
            $nextDay = date("Y-m-d", strtotime("+1 day", strtotime($nextDay)));
        } while ($holiday_control == '1' && !($this->chkBussinessDay($nextDay)));

        return $nextDay;
    }

    //曜日指定 日付取得
    //引数：bean
    public function getDayWeekDay($teirei_weeks, $holiday_control, $last_day, $new_flag = false) {
        $nextDay = $last_day;
        if($new_flag && (in_array(date('w', strtotime($nextDay)), $teirei_weeks)) && ($holiday_control == '0' || $this->chkBussinessDay($nextDay))){
            return $nextDay;
        }
        do {
            $nextDay = date("Y-m-d", strtotime("+1 day", strtotime($nextDay)));
//            $GLOBALS['log']->fatal(date('w', strtotime($nextDay)));
        } while (!(in_array(date('w', strtotime($nextDay)), $teirei_weeks)) || ($holiday_control == '1' && !($this->chkBussinessDay($nextDay))));

        return $nextDay;
    }

    //毎月定例日： 1日～31日の場合
    public function getNextMonthDay($teirei_day, $holiday_control, $last_day, $new_flag = false) {
        $nextDay = $last_day;
        
        $str_len = strlen($teirei_day); //長さ取得
        if ($str_len === 1) {
            //長さが１ケタの場合
            $teirei_day = '0' . $teirei_day; //前ゼロ付加
        }
        
        if($new_flag){
            $arr_last_day = date_parse($nextDay);
            if ($arr_last_day["day"] < $teirei_day) {
                $dateFormat = "Y-m-" . $teirei_day;
                $nextDay = date($dateFormat, strtotime(date($arr_last_day["year"] . "-" . $arr_last_day["month"] . "-01")));
                if($holiday_control == '0' || ($this->chkBussinessDay($nextDay))){
                    $arr_nextDay = date_parse($nextDay);
                    if (!checkdate($arr_nextDay['month'], $arr_nextDay['day'], $arr_nextDay['year'])) {
                        $nextDay = date("Y-m-t", strtotime(date($arr_nextDay["year"] . "-" . $arr_nextDay["month"] . "-01")));
                    }
                return $nextDay;
                }
            }elseif($arr_last_day["day"] == $teirei_day){
                if($holiday_control == '0' || ($this->chkBussinessDay($nextDay))){
                return $nextDay;
                }
            }
        }        
        
        do {
            $arr_last_day = date_parse($nextDay);

            $dateFormat = "Y-m-" . $teirei_day; //dateフォーマット
            $nextDay = date($dateFormat, strtotime(date($arr_last_day["year"] . "-" . $arr_last_day["month"] . "-01") . "+1 month")); //今月１日から来月の指定日付取得
            $arr_nextDay = date_parse($nextDay);
            if (!checkdate($arr_nextDay['month'], $arr_nextDay['day'], $arr_nextDay['year'])) {
                $nextDay = date("Y-m-t", strtotime(date($arr_nextDay["year"] . "-" . $arr_nextDay["month"] . "-01")));
            }
        } while ($holiday_control == '1' && !($this->chkBussinessDay($nextDay)));

        return $nextDay;
    }

    //営業日判定
    public function getBusinessDay($teirei_businessday, $last_day, $new_flag = false) {

        $arr_last_day = date_parse($last_day);
        $nextDay = date("Y-m-01", strtotime(date($arr_last_day["year"] . "-" . $arr_last_day["month"] . "-01") . "+1 month")); //今月１日から来月の指定日付取得

        $cnt = 0;
        if ($this->chkBussinessDay($nextDay)) {
            $cnt++;
        }

        while ($cnt < $teirei_businessday) {
            $nextDay = date("Y-m-d", strtotime("+1 day", strtotime($nextDay)));
            if ($this->chkBussinessDay($nextDay)) {
                $cnt++;
            }
        }
        return $nextDay;
    }

    //隔週 日付取得
    //引数：bean
    //※初回実行日が入力されていなかった時の動き⇒必須フィールドにすればＯＫ？
    public function getBiweeklyDay($teirei_week, $last_day, $holiday_control, $new_flag = false) {

        $nextDay = $last_day;
        if($new_flag){
            if (!($teirei_week == date('w', strtotime($nextDay)))) {
                $nextDay = date('Y-m-d', strtotime("next " . $this->engWeekday[$teirei_week], strtotime($nextDay)));
            }
            if($holiday_control == '0' || ($this->chkBussinessDay($nextDay))){
                return $nextDay;
            }
        }
        do {
            if (!($teirei_week == date('w', strtotime($nextDay)))) {
                $nextDay = date('Y-m-d', strtotime("next " . $this->engWeekday[$teirei_week], strtotime($nextDay)));
            }
            $nextDay = date("Y-m-d", strtotime("+2 week", strtotime($nextDay)));
            $GLOBALS['log']->fatal("#####".$nextDay.$this->chkBussinessDay($nextDay));
        } while ($holiday_control == '1' && !($this->chkBussinessDay($nextDay)));
        $GLOBALS['log']->fatal("#####".$nextDay);

        return $nextDay;
    }

    //第〇週 日付取得
    //引数：bean
    public function getnWeekDay($teirei_nweek, $teirei_week, $last_day, $holiday_control, $new_flag) {
        $nextDay = $last_day;
        
        if($new_flag && (in_array($this->chknWeekDay($nextDay, $teirei_week), $teirei_nweek) && ($holiday_control == '0' || $this->chkBussinessDay($nextDay)))){
            if ($teirei_week == date('w', strtotime("$nextDay"))) {
                return $nextDay;
            }    
            
        }

        do {
            if (!($teirei_week == date('w', strtotime("$nextDay")))) {
                $nextDay = date('Y-m-d', strtotime("next " . $this->engWeekday[$teirei_week], strtotime($nextDay)));
            } else {
                $nextDay = date("Y-m-d", strtotime("+1 week", strtotime($nextDay)));
            }
//            $GLOBALS['log']->fatal($this->chknWeekDay($nextDay, $teirei_week));
        } while (!(in_array($this->chknWeekDay($nextDay, $teirei_week), $teirei_nweek) && ($holiday_control == '0' || $this->chkBussinessDay($nextDay))));

        return $nextDay;
    }

    private function chknWeekDay($nextDay, $teirei_week) {
        $arr_last_day = date_parse($nextDay);
        $w = date("w", strtotime(date($arr_last_day["year"] . "-" . $arr_last_day["month"] . "-01")));

        //指定された曜日の最初の日
        $first = ($teirei_week - $w >= 0) ? 1 + $teirei_week - $w : 1 + $teirei_week - $w + 7;
        //日にちを算出

        return ($arr_last_day["day"] - $first) / 7 + 1;
    }

    public function chkBussinessDay($chkDay) {
        if ($this->chkWeekDay($chkDay)) {
            if ($this->chkHolidayM($chkDay)) {
                return false;
            }else{
                return true;
            }
        }
        return false;
    }

    //曜日判定
    //@$weekday_flg : true : 平日, false : 土日
    private function chkWeekDay($chkDay) {
        $weekday_flg = true; //true : 平日 , false : 土日

        $weekday_day = date('w', strtotime($chkDay)); //曜日取得
//                $GLOBALS['log']->fatal('■■'.$weekday_day.'■■■');
        //取得曜日が土日だったら
        if ($weekday_day == '0' || $weekday_day == '6') {
            $weekday_flg = false;
        }


        return $weekday_flg;
    }

    //休日マスター検索
    //休日マスターにデータの存在チェックをかける
    private function chkHolidayM($chkNextDay) {

        $exist_flg = "";    //休日データ存在フラグ
        $holidayM = new ZK7_HolidayM(); //休日マスターインスタンス

        $order = "";
        $where = sprintf("holiday = '%s'", $chkNextDay);
        $holidayM_list = $holidayM->get_full_list($order, $where);
        if ($holidayM_list) {
            //休日データ存在
            $exist_flg = true;
        } else {
            //休日データ無し
            $exist_flg = false;
        }

        return $exist_flg;
    }

}

?>